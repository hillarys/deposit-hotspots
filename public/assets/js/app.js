// API URL
var apiPostcode = "https://www.smclogistics.co.uk/postcodesbydeposits/getdata.php?postcode=";

// MAP GLOBAL
var markerArray = [];
var mapLoaded = false;

// RAINBOW
var rainbow = new Rainbow();
rainbow.setSpectrum('0cc9c5', '578bdf', 'ce2286');
rainbow.setNumberRange(0, 150);

// MAP
var map;

var $main_color = '#111925',
    $saturation = -20,
    $brightness = 30;
var style = [{
        //set saturation for the labels on the map
        elementType: "labels",
        stylers: [{
            saturation: $saturation
        }]
    }, { //poi stands for point of interest - don't show these lables on the map
        featureType: "poi",
        elementType: "labels",
        stylers: [{
            visibility: "off"
        }]
    }, {
        //don't show highways lables on the map
        featureType: 'road.highway',
        elementType: 'labels',
        stylers: [{
            visibility: "off"
        }]
    }, {
        //don't show local road lables on the map
        featureType: "road.local",
        elementType: "labels.icon",
        stylers: [{
            visibility: "off"
        }]
    }, {
        //don't show arterial road lables on the map
        featureType: "road.arterial",
        elementType: "labels.icon",
        stylers: [{
            visibility: "off"
        }]
    }, {
        //don't show road lables on the map
        featureType: "road",
        elementType: "geometry.stroke",
        stylers: [{
            visibility: "off"
        }]
    },
    //style different elements on the map
    {
        featureType: "transit",
        elementType: "geometry.fill",
        stylers: [{
            hue: $main_color
        }, {
            visibility: "on"
        }, {
            lightness: $brightness
        }, {
            saturation: $saturation
        }]
    }, {
        featureType: "poi",
        elementType: "geometry.fill",
        stylers: [{
            hue: $main_color
        }, {
            visibility: "on"
        }, {
            lightness: $brightness
        }, {
            saturation: $saturation
        }]
    }, {
        featureType: "poi.government",
        elementType: "geometry.fill",
        stylers: [{
            hue: $main_color
        }, {
            visibility: "on"
        }, {
            lightness: $brightness
        }, {
            saturation: $saturation
        }]
    }, {
        featureType: "poi.sport_complex",
        elementType: "geometry.fill",
        stylers: [{
            hue: $main_color
        }, {
            visibility: "on"
        }, {
            lightness: $brightness
        }, {
            saturation: $saturation
        }]
    }, {
        featureType: "poi.attraction",
        elementType: "geometry.fill",
        stylers: [{
            hue: $main_color
        }, {
            visibility: "on"
        }, {
            lightness: $brightness
        }, {
            saturation: $saturation
        }]
    }, {
        featureType: "poi.business",
        elementType: "geometry.fill",
        stylers: [{
            hue: $main_color
        }, {
            visibility: "on"
        }, {
            lightness: $brightness
        }, {
            saturation: $saturation
        }]
    }, {
        featureType: "transit",
        elementType: "geometry.fill",
        stylers: [{
            hue: $main_color
        }, {
            visibility: "on"
        }, {
            lightness: $brightness
        }, {
            saturation: $saturation
        }]
    }, {
        featureType: "transit.station",
        elementType: "geometry.fill",
        stylers: [{
            hue: $main_color
        }, {
            visibility: "on"
        }, {
            lightness: $brightness
        }, {
            saturation: $saturation
        }]
    }, {
        featureType: "landscape",
        stylers: [{
            hue: $main_color
        }, {
            visibility: "on"
        }, {
            lightness: $brightness
        }, {
            saturation: $saturation
        }]

    }, {
        featureType: "road",
        elementType: "geometry.fill",
        stylers: [{
            hue: $main_color
        }, {
            visibility: "on"
        }, {
            lightness: $brightness
        }, {
            saturation: $saturation
        }]
    }, {
        featureType: "road.highway",
        elementType: "geometry.fill",
        stylers: [{
            hue: $main_color
        }, {
            visibility: "on"
        }, {
            lightness: $brightness
        }, {
            saturation: $saturation
        }]
    }, {
        featureType: "water",
        elementType: "geometry",
        stylers: [{
            hue: $main_color
        }, {
            visibility: "on"
        }, {
            lightness: $brightness
        }, {
            saturation: $saturation
        }]
    }
];

// FACEBOOK
function facebookHandler() {
    FB
        .ui({
            method: 'feed',
            link: 'https://www.hillarys.co.uk/static/deposit-hotspots/',
            name: 'Deposit Hotspots | Hillarys',
            description: 'Looking to get on the property ladder? Pop in your postcode and we’ll tell you how long it’ll take you to save a deposit.',
            caption: 'hillarys.co.uk'
        }, function (response) {});
}

$(".share-facebook").click(function () {
    facebookHandler();
  });

function loadingScreen(status) {

    if (status) {
        $('#btn-text').hide();
        $('.loader').show();
    } else {
        $('#btn-text').show();
        $('.loader').hide();
    }
}

function getCurrentLocation(latlng) {

    geocoder
        .geocode({
            'location': latlng
        }, function (results, status) {
            if (status === 'OK') {
                if (results[1]) {
                    console.log(results[1]);
                    var area;

                    for (var i = 0; i < results[0].address_components.length; i++) {
                        var component = results[0].address_components[i];
                        if (component.types[0] == "postal_code") {
                            area = component.long_name;
                            break;
                        }
                    }
                }
            }
        });

}

$(document)
    .ready(function () {

        new autoComplete({
            selector: 'input[name="input-area"]',
            minChars: 1,
            source: function (term, suggest) {
                term = term.toLowerCase();
                var matches = [];
                for (i = 0; i < places.length; i++)
                    if (~places[i].toLowerCase().indexOf(term))
                        matches.push(places[i]);
                suggest(matches);
            }
        });

        $("#btn-calculate").click(function () {
            var inputName = $('#input-box').val();
            makeCall(inputName);
            loadingScreen(true);
        });
    });

function makeCall(area) {
    var url = apiPostcode + area;
    $.getJSON(url, function (data) {
        loadingScreen(false);

        if(data.areaArray == null){
            $('#input-box').val('');
            $('#error-message').show();
            return
        }

            $('#section-deposit, #section-stats-city, #section-about, #section-map, #section-stats-postcode, #section-share').show();
            $('#error-message').fadeOut();

            if(area.toLowerCase() == (data.locationName.City).toLowerCase()){
                $('#area-name').text(data.locationName.City);
            } else {
                $('#area-name').text(area.toUpperCase());
            }

            var totalYears;
            var decimals;
            var totalMonths;
            var sortedArray = data.areaArray.sort(compare);
            // console.log(sortedArray);
            $(".dynamic-graph-row").empty();
            var newArray = data.areaArray;

            if(data.areaArray.length >= 20){
                $('.row-right').show();
                $('.desktop-left').css('width', '49%');
                $('.two-title-column').show();
                $('.one-title-column').hide();

                // add to two columns
                var quickest10 = newArray.slice(0, 10);
                var longest10 = newArray.slice(Math.max(newArray.length - 10, 1));
                longest10 = longest10.reverse();

                addToColumns(quickest10, $('.row-left'), area);
                addToColumns(longest10, $('.row-right'), area);

            } else {

                $('.row-right').hide();
                $('.desktop-left').css('width', '100%');
                $('.two-title-column').hide();
                $('.one-title-column').show();


                // add to one column
                addToColumns(newArray, $('.row-left'), area);
            }
            
            // If general location or specific postcode conditionals
            var total = 0;

            if(area.toLowerCase() == (data.locationName.City).toLowerCase()){
                // Work out average of all locations
                $.each(data.areaArray, function (index, value) {
                    total = total + Number(value.PerHour);
                });
                total = (total / data.areaArray.length).toFixed(2);
            } else {
                // Find specific area code
                $.each(data.areaArray, function (index, value) {
                    var prefix = area.split(" ")[0];
                    if(prefix.toLowerCase() == value.Postcode.toLowerCase()){
                        total = Number(value.PerHour).toFixed(2);
                    }
                });
                if(total == 0){
                    var postcode4 = area.toLowerCase().substring(0, 4);
                    console.log(postcode4);
                    $.each(data.areaArray, function (index, value) {
                        if(postcode4.toLowerCase() == value.Postcode.toLowerCase()){
                            total = Number(value.PerHour).toFixed(2);
                        }
                    });
                }

                if(total == 0){
                    var postcode3 = area.toLowerCase().substring(0, 3);
                    console.log(postcode3);
                    $.each(data.areaArray, function (index, value) {
                        if(postcode3.toLowerCase() == value.Postcode.toLowerCase()){
                            total = Number(value.PerHour).toFixed(2);
                        }
                    });
                }
            }
            // Get total Years
            totalYears = Math.floor(total).toFixed(0);
            
            // Get total Months
            decimals = total - Math.floor(total);
            totalMonths = Math.round(decimals.toFixed(1)*12);
            
            if(totalMonths == 0){
                $("#totalTime").text(totalYears + " Years");
            } else {
                $("#totalTime").text(totalYears + " Years and " + totalMonths + " Months");
            }

            if(totalMonths == 12){
                totalMonths = 11;
            }


            // Immediately sets the location of the map
            setMapCenter(data.locationName);

            // Animates to the map section and allows custom markers to pop in
            $("html, body").animate({
                scrollTop: $('#section-deposit')
                    .offset()
                    .top
            }, 700, function () {
                updateMap(data);
            });
        })
        .fail(function () {
            console.log("error");
        });
}

function addToColumns(array, dom, area){
    
    $.each(array, function (index, value) {
        // add to dynamic grid
        var dateVal = Number(value.PerHour);
        var date = convertToDate(dateVal);
        var i = index+1;
        var li = '<li class="ui-menu-item" role="menuitem"><strong>'+ i + ". " + value.Postcode+"</strong> - " +date[0]+ " " + date[1] + '</li>';
        dom.append(li);       

        var prefix = area.split(" ")[0];
    });

}

function compare(a,b) {
    var num1 = Number(a.PerHour);
    var num2 = Number(b.PerHour);

    if (num1 < num2)
      return -1;
    if (num1 > num2)
      return 1;
    return 0;
  }

function updateMap(data) {
    var location = data.locationName;
    var mapData = data.areaArray;
    var positions = data.positions;
    addMarkers(mapData);
}

function setMapCenter(locationInfo) {
    map.setCenter(new google.maps.LatLng(locationInfo.Lattitude, locationInfo.Longitude));
    if (locationInfo.City == 'London') {
        map.setZoom(14);
    } else {
        map.setZoom(11);
    }
}

function initMap() {

    var geocoder = new google.maps.Geocoder;
    var objArray = [];
    var minZoomLevel = 10;
    var maxZoomLevel = 15;
    var startPos = {
        lat: 58.018282,
        lng: -10.458984
    };
    var mapOptions = {
        center: startPos,
        zoom: minZoomLevel,
        gestureHandling: 'cooperative',
        disableDefaultUI: true,
        zoomControl: true,
        styles: style
    };

    map = new google
        .maps
        .Map(document.getElementById('map'), mapOptions);

    google
        .maps
        .event
        .addListener(map, 'zoom_changed', function () {
            if (map.getZoom() < minZoomLevel)
                map.setZoom(minZoomLevel);
            if (map.getZoom() > maxZoomLevel)
                map.setZoom(maxZoomLevel);
        });

    // //ensure google library is loaded before running the custom marker method
    loadMarkers();
}

//Custom Marker method & prototypal inheritance
function CustomMarker(latlng, map, args) {
    //object properties
    this.latlng = latlng;
    this.args = args;
    this.setMap(map);
}

updateGrids();
//Update the grid

function updateGrids(){
    $( ".desktop-list-items" ).each(function( index ) {

        var desktopli = $(this).find(".graph-row");
        $( desktopli ).each(function( index ) {
        var first = $(this).find("li")[0];
        var firstData = $(first).data("src");
        var firstTitle = $(first).data("title");
        var firstDate = convertToDate(firstData);

        var second = $(this).find("li")[1];
        var secondData = $(second).data("src");
        var secondTitle = $(second).data("title");
        var secondDate = convertToDate(secondData);
        var i = index+1;


        $(first).html( "<strong>"+i+". "+firstTitle + " - </strong> " + firstDate[0] + " " + firstDate[1]);
        $(second).html( "<strong>"+i+". "+secondTitle + " - </strong> " + secondDate[0] + " " + secondDate[1]);
        
        // var minWidth = 19;
        // var max = secondData;
        // var difference = secondData-firstData;
        // var percentageLeft = (difference / 3)*50;
        // var percentageRight = (97-percentageLeft);

        // $(first).css('width', '49%');
        // $(second).css('width', '49%');
    });

    $( ".mobile-graph-row" ).each(function( index ) {
        var mobileli = $(this).find("li");

        $( mobileli ).each(function( index ) {
            var mobileTitle = $(this).data("title");
            var data = $(this).data("src");
            var dataTime = convertToDate(data);
            $(this).html("<strong>"+(index+1) +". "+ mobileTitle +"</strong> " + dataTime[0] + " " + dataTime[1]);
        });
    });
});
}

function convertToDate(dateNo){
    var convDate = parseFloat(dateNo).toFixed(2);
    var year = Math.trunc(parseFloat(convDate));
    var decimal = convDate - Math.floor(convDate);
    decimal = decimal.toFixed(1);
    var month = Math.round(12 * decimal);

    // console.log(convDate);
    // console.log(year, decimal, month);
    if(month == 12){
        month = 11;
    }

    if(year == 1 || year == 0){
        year = year + " year"
    } else {
        year = year + " years"
    }

    if(month == 0){
        month = "";
    } else if(month == 1){
        month = "and " + month + " month"
    } else {
        month = "and " + month + " months"
    }

    return [year, month]    
}

$("body")
    .on("transitionend MSTransitionEnd webkitTransitionEnd oTransitionEnd", function () {
        $(this).removeClass("animated bounceIn");
    });

function loadMarkers() {

    //extend overlayview maps
    CustomMarker.prototype = new google
        .maps
        .OverlayView();
    CustomMarker.prototype.draw = function () {

        var self = this;
        var div = this.div;

        if (!div) {
            var self = this;
            div = this.div = document.createElement('div');
            el = $(div);
            el.addClass('customMarker clearfix price animated bounceIn');
            el.html('<span>' + this.args.postcode + '</span><br><strong>' + Math.round(this.args.price) + ' years</strong>');
            el.css('background-color', '#' + this.args.color);

            // Add animation
            var panes = this.getPanes();
            panes
                .overlayImage
                .appendChild(div);
        }

        var point = this
            .getProjection()
            .fromLatLngToDivPixel(this.latlng);

        if (point) {
            div.style.left = (point.x - 10) + 'px';
            div.style.top = (point.y - 20) + 'px';
        }
    };

    CustomMarker.prototype.remove = function () {
        if (this.div) {
            this
                .div
                .parentNode
                .removeChild(this.div);
            this.div = null;
        }
    };

    CustomMarker.prototype.getPosition = function () {
        return this.latlng;
    };
}

function addMarkers(arry) {

    removeMarkers();

    var timerDelay = 0;
    var addition = 100;

    if (arry.length >= 50) {
        addition = 10;
    } else if (arry.length >= 20) {
        addition = 40;
    }

    $
        .each(arry, function () {
            var obj = this;
            setTimeout(function () {
                overlay = new CustomMarker(new google.maps.LatLng(obj.Lattitude, obj.Longitude), map, {
                    postcode: obj.Postcode,
                    price: obj.PerHour,
                    color: rainbow.colourAt(obj.PerHour)
                });
                markerArray.push(overlay);
            }, timerDelay);

            timerDelay += addition;
        });

    google
        .maps
        .event
        .trigger(map, 'resize');
}

function removeMarkers() {
    $
        .each(markerArray, function () {
            this.remove();
        })

    markerArray = [];
}

$('#graph-button1').click(function(){
    $('#graph-1').slideToggle();
});

$('#graph-button2').click(function(){
    $('#graph-2').slideToggle();
});